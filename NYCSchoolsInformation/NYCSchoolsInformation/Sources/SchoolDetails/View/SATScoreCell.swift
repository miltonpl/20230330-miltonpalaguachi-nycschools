//
//  SchoolDetailsCell.swift
//  NYCSchoolsInformation
//
//  Created by Milton Palaguachi on 3/30/23.
//

import UIKit

class SATScoreCell: UICollectionViewCell {
    enum Layout {
        static let space16: CGFloat = 16
        static let space8: CGFloat = 8
    }

    private var titleLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.font = .systemFont(ofSize: 14, weight: .bold)
       return label
    }()

    private var subtitleLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .right
        label.font = .systemFont(ofSize: 14, weight: .light)
       return label
    }()

    var model: Model = .init() {
        didSet {
            applyModel()
        }
    }

    public override init(frame: CGRect) {
        super.init(frame: frame)
        assemble()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func assembleView() {
        contentView.backgroundColor = .systemGray5
        contentView.addAutolayoutView(titleLabel)
        contentView.addAutolayoutView(subtitleLabel)
    }

    func assembleSubviewConstrains() {
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: Layout.space8),
            titleLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: Layout.space8),
            titleLabel.trailingAnchor.constraint(equalTo: subtitleLabel.leadingAnchor, constant: Layout.space8),
            titleLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -Layout.space8),
            subtitleLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: Layout.space8),
            subtitleLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -Layout.space8),
            subtitleLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -Layout.space8),
            ]
        )
    }

    private func applyModel() {
        titleLabel.text = model.title
        subtitleLabel.text = model.subtitle
    }
}

extension SATScoreCell {
    struct Model: Hashable {
        var title: String?
        var subtitle: String?
    }

    func assemble() {
        assembleView()
        assembleSubviewConstrains()
    }
}
