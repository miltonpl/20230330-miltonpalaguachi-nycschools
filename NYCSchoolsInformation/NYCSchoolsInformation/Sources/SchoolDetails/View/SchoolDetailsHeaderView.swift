//
//  SchoolDetailsHeaderView.swift
//  NYCSchoolsInformation
//
//  Created by Milton Palaguachi on 3/30/23.
//

import UIKit

class SchoolDetailsHeaderView: UICollectionReusableView {
    static var identifier = String(describing: SchoolDetailsHeaderView.self)

    let titlelabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 17, weight: .medium)
        label.textColor = .label
        label.adjustsFontForContentSizeCategory = true
        return label
    }()

    public override init(frame: CGRect) {
        super.init(frame: frame)
        assembleView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        var layoutBounds = bounds.inset(by: layoutMargins)
        let fittingSize = CGSize(width: layoutBounds.width, height: UILabel.noIntrinsicMetric)
        let size = titlelabel.sizeThatFits(fittingSize)
        (titlelabel.frame, layoutBounds) = layoutBounds.divided(
            atDistance: size.height,
            from: .minYEdge
        )
    }
    override func sizeThatFits(_ size: CGSize) -> CGSize {
        let layoutMargin = layoutMargins
        let fittingSize = CGSize(
            width: size.width - layoutMargin.left - layoutMargin.right,
            height: UILabel.noIntrinsicMetric
        )
        var height: CGFloat = layoutMargin.top + layoutMargin.bottom
        height += titlelabel.sizeThatFits(fittingSize).height
        return CGSize(width: size.width, height: height)
    }

    override func preferredLayoutAttributesFitting(
        _ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        // This will call `sizeThatFits`
        return super.preferredLayoutAttributesFitting(layoutAttributes)
    }

    func assembleView() {
        addSubview(titlelabel)
    }
}
