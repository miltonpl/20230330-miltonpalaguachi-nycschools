//
//  HighSchoolCell.swift
//  NYCSchoolsInformation
//
//  Created by Milton Palaguachi on 3/29/23.
//

import UIKit

class HighSchoolCell: UICollectionViewCell {
    enum Layout {
        static let space16: CGFloat = 16
        static let space8: CGFloat = 8
    }

    struct Model: Hashable {
        var title: String?
        var subtitle: String?
    }

    private var titleLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.font = .boldSystemFont(ofSize: 14)
        label.textColor = .label
        label.text = .localized(.highSchoolName)
       return label
    }()
    
    private var subtitleLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.font = .systemFont(ofSize: 14, weight: .light)
        label.textColor = .label
        label.numberOfLines = 0
        label.text = .localized(.highSchoolOverview)
       return label
    }()

    var model: Model = .init() {
        didSet {
            applyModel()
        }
    }

    public override init(frame: CGRect) {
        super.init(frame: frame)
        assemble()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func assembleView() {
        contentView.backgroundColor = .white
        contentView.addAutolayoutView(titleLabel)
        contentView.addAutolayoutView(subtitleLabel)
        contentView.backgroundColor = .systemGray5
    }
  
    func assembleSubviewConstrains() {
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: Layout.space8),
            titleLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: Layout.space16),
            titleLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -Layout.space16),
            subtitleLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: Layout.space8),
            subtitleLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: Layout.space16),
            subtitleLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -Layout.space16),
            subtitleLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -Layout.space8),
        ])
        
        titleLabel.setContentCompressionResistancePriority(.required, for: .vertical)
        subtitleLabel.setContentCompressionResistancePriority(.defaultLow, for: .vertical)
    }

    private func applyModel() {
        titleLabel.text = model.title
        subtitleLabel.text = model.subtitle
    }
}

extension HighSchoolCell {
    func assemble() {
        assembleView()
        assembleSubviewConstrains()
    }
}

extension UIView {
    
    public func addAutolayoutView(_ view: UIView) {
        view.translatesAutoresizingMaskIntoConstraints = false
        addSubview(view)
    }
}
