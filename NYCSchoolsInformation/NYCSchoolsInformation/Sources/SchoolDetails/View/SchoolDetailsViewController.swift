//
//  SchoolDetailsViewController.swift
//  NYCSchoolsInformation
//
//  Created by Milton Palaguachi on 3/30/23.
//

import UIKit

extension SchoolDetailsViewController {
    
    enum Item: Hashable {
        case sat(SATScoreCell.Model)
        case detail(HighSchoolCell.Model)
    }
    
    struct Section: Hashable {
        enum Identifier: String {
            case sat = "STA Average Scores"
            case details = "School details"
        }

        let identifier: Identifier
    }
    
    typealias DataSource = UICollectionViewDiffableDataSource<Section, Item>
    typealias Snapshot = NSDiffableDataSourceSnapshot<Section, Item>
}

class SchoolDetailsViewController: UIViewController {
    struct Data {
        var satScoreModel: SATScoreModel
        var highSchoolModel: HighSchoolModel
    }
    enum Layout {
        static let heightDimension: CGFloat = 40
        static let itemFractionalWidth: CGFloat = 1.0
        static let itemSpacing: CGFloat = 1.0
        static let sectionSpacing: CGFloat = 5.0
    }

    private lazy var collectionView: UICollectionView = {
        let collectionView = UICollectionView(frame: view.bounds, collectionViewLayout: layout())
        collectionView.backgroundColor = .systemBackground
        return collectionView
    }()

    private lazy var dataSource = configureDataSource()

    var data: Data

    init(_ satScoreModel: SATScoreModel, _ highSchoolModel: HighSchoolModel) {
        data = .init(satScoreModel: satScoreModel, highSchoolModel: highSchoolModel)
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        title = "School Details"
        assemble()
        applySections()
    }

    func assembleView() {
        view.addAutolayoutView(collectionView)
    }

    func assembleSubviewConstrains() {
        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            collectionView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            collectionView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
            collectionView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
        ])
    }

    private func applySections(animated: Bool = false) {
        var snapshot = Snapshot()
        let satSection = Section(identifier: .sat)
        snapshot.appendSections([satSection])
        snapshot.appendItems(data.satScoreModel.satScoreModel.map{ .sat($0) }, toSection: satSection)

        let detailsSection = Section(identifier: .details)
        snapshot.appendSections([detailsSection])
        snapshot.appendItems(data.highSchoolModel.models.map{ .detail($0) }, toSection: detailsSection)

        dataSource.apply(snapshot, animatingDifferences: animated)
    }
}

extension SchoolDetailsViewController {

    private func layout() -> UICollectionViewLayout {
        let sectionProvider = {(sectionIndex: Int, _: NSCollectionLayoutEnvironment)
            -> NSCollectionLayoutSection? in
            let section: NSCollectionLayoutSection = .list(
                size: .init(
                    width: .fractionalWidth(Layout.itemFractionalWidth),
                    height: .estimated(Layout.heightDimension)
                ),
                itemSpacing: Layout.itemSpacing,
                sectionContentInsets: .init(literalFloat: Layout.sectionSpacing)
            )
            let titleSize = NSCollectionLayoutSize(
                widthDimension: .fractionalWidth(Layout.itemFractionalWidth),
                heightDimension: .estimated(17)
            )
            let titleSupplementary = NSCollectionLayoutBoundarySupplementaryItem(
                layoutSize: titleSize,
                elementKind: SchoolDetailsHeaderView.identifier,
                alignment: .top
            )
            section.boundarySupplementaryItems = [titleSupplementary]
            return section
        }
        let config = UICollectionViewCompositionalLayoutConfiguration()
        config.interSectionSpacing = Layout.itemSpacing
        let layout = UICollectionViewCompositionalLayout(sectionProvider: sectionProvider, configuration: config)
        return layout
    }

    func configureDataSource() -> DataSource {
        let cellRegistration1 = UICollectionView.CellRegistration<SATScoreCell, SATScoreCell.Model> { cell, indexPath, item in
            cell.model = item
        }
        let cellRegistration2 = UICollectionView.CellRegistration<HighSchoolCell, HighSchoolCell.Model> { cell, indexPath, item in
            cell.model = item
        }

        let dataSource = DataSource(collectionView: collectionView) { collectionView, indexPath, item in
            switch item {
                case .sat(let model):
                    return collectionView
                        .dequeueConfiguredReusableCell(using: cellRegistration1, for: indexPath, item: model)
                case .detail(let model):
                    return collectionView
                        .dequeueConfiguredReusableCell(using: cellRegistration2, for: indexPath, item: model)
           }
        }
    
        let headerRegistration = sectionHeaderRegistration()
        dataSource.supplementaryViewProvider = { collectionView, _, indexPath in
            return collectionView.dequeueConfiguredReusableSupplementary(
                using: headerRegistration, for: indexPath
            )
        }
    
        return dataSource
    }
    private func sectionHeaderRegistration() -> UICollectionView.SupplementaryRegistration<SchoolDetailsHeaderView> {
        let headerRegistration = UICollectionView.SupplementaryRegistration<SchoolDetailsHeaderView>(
            elementKind: SchoolDetailsHeaderView.identifier) { [weak self] supplementaryView, _, indexPath in
                let sectionTitle = self?.dataSource.snapshot().sectionIdentifiers[indexPath.section].identifier
                supplementaryView.titlelabel.text = sectionTitle?.rawValue
                supplementaryView.backgroundColor = .clear
            }
        return headerRegistration
    }
}

extension SchoolDetailsViewController {
    // These funcs should be move a base class
    func assemble() {
        assembleView()
        assembleSubviewConstrains()
    }
}
