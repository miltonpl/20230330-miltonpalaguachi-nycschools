//
//  NSCollectionLayoutSection+Ext.swift
//  NYCSchoolsInformation
//
//  Created by Milton Palaguachi on 3/29/23.
//

import UIKit

extension NSCollectionLayoutSection {
    static func itemLayout(itemDimension: LayoutDimension) -> NSCollectionLayoutItem {
        let itemSize = NSCollectionLayoutSize(
            widthDimension: itemDimension.width,
            heightDimension: itemDimension.height
        )
        return NSCollectionLayoutItem(layoutSize: itemSize)
    }

    static func list(size: LayoutDimension,
                     itemSpacing: CGFloat = .zero,
                     sectionContentInsets: NSDirectionalEdgeInsets
    ) -> NSCollectionLayoutSection {
        let item = itemLayout(itemDimension: size)
        let groupSize = NSCollectionLayoutSize(
            widthDimension: size.width,
            heightDimension: size.height
        )
        let group = NSCollectionLayoutGroup
            .horizontal(layoutSize: groupSize,
                        subitems: [item]
            )
        group.interItemSpacing = .fixed(itemSpacing)
        let section = NSCollectionLayoutSection(group: group)
        section.supplementariesFollowContentInsets = false
        section.interGroupSpacing = itemSpacing
        section.contentInsets = sectionContentInsets
        return section
    }
    
}

extension NSDirectionalEdgeInsets {
    init(literalFloat: CGFloat) {
        self.init(
            top: literalFloat,
            leading: literalFloat,
            bottom: literalFloat,
            trailing: literalFloat
        )
    }
}
