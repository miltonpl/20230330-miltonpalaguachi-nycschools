//
//  Coordinator.swift
//  NYCSchoolsInformation
//
//  Created by Milton Palaguachi on 3/29/23.
//

import Foundation

public protocol Coordinator {
    func start()
}
