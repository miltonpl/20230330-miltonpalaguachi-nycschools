//
//  LayoutDimension.swift
//  NYCSchoolsInformation
//
//  Created by Milton Palaguachi on 3/29/23.
//

import UIKit

public struct LayoutDimension {
    public let width: NSCollectionLayoutDimension
    public let height: NSCollectionLayoutDimension

    public init(width: NSCollectionLayoutDimension, height: NSCollectionLayoutDimension) {
        self.width = width
        self.height = height
    }
}
