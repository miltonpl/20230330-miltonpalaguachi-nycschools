//
//  PresentingStyle.swift
//  NYCSchoolsInformation
//
//  Created by Milton Palaguachi on 3/29/23.
//

import UIKit

public enum PresentingStyle {
    case present(presenter: UIViewController, animated: Bool)
    case push(navigationController: UINavigationController, animated: Bool)
}
