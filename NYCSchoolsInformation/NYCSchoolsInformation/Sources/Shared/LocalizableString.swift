//
//  LocalizableString.swift
//  NYCSchoolsInformation
//
//  Created by Milton Palaguachi on 3/29/23.
//

import Foundation

enum LocalizableString {
    case highSchoolsTitle
    case highSchoolName
    case highSchoolOverview
}

extension LocalizableString {
    private var key: String {
        let key = String(describing: self)
        if let index = key.firstIndex(of: "(") {
            return String(key[..<index])
        }
        return key
    }

    var value: String {
        let value = NSLocalizedString(key, tableName: "Localizable", bundle: .main, comment: "")
        precondition(value != key, "No localized string found for `\(key)`")

        switch self {
            default:
                return value
        }
    }
}
extension String {
    static func localized(_ localize: LocalizableString) -> String {
        localize.value
    }
}
