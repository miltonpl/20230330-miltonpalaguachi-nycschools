//
//  Networking.swift
//  NYCSchoolsInformation
//
//  Created by Milton Palaguachi on 3/29/23.
//

import Foundation
import Combine

// We can improve this class to handle multiple call made to URLSession

class Networking {
    let session: URLSession
    
    init(session: URLSession) {
        self.session = session
    }

    public func request<Model: Decodable>(request: URLRequest, modelType: Model.Type) -> AnyPublisher<Model, Error> {
        responsePublisher(for: request)
            .map(\.data)
            .decode(type: modelType.self, decoder: JSONDecoder())
            .eraseToAnyPublisher()
    }
}

private extension Networking {
    func responsePublisher(for request: URLRequest) -> AnyPublisher<URLSession.DataTaskPublisher.Output, Error> {
        return session.dataTaskPublisher(for: request).mapError { $0 as Error }.eraseToAnyPublisher()
    }
}
