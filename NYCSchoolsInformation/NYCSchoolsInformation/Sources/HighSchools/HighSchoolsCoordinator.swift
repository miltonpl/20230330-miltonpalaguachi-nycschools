//
//  HighSchoolsCoordinator.swift
//  NYCSchoolsInformation
//
//  Created by Milton Palaguachi on 3/29/23.
//

import UIKit
import Combine

class HighSchoolsCoordinator: Coordinator {
    var navigationController: UINavigationController {
        switch presentingStyle {
            case .present(let presenter, _):
                return UINavigationController(rootViewController: presenter)
            case .push(let navigationController, _):
                return navigationController
        }
    }

    private var presentingStyle: PresentingStyle
    private let highSchoolsViewController: HighSchoolsViewController
    private let highSchoolsDataSource: HighSchoolDataSourceAPI
    private var subscribers = Set<AnyCancellable>()

    init(presentingStyle: PresentingStyle) {
        self.presentingStyle = presentingStyle
        highSchoolsViewController = HighSchoolsViewController(title: .localized(.highSchoolsTitle))
        highSchoolsDataSource = HighSchoolDataSource()
    }
    
    func start() {
        highSchoolsViewController.delegate = self
        switch presentingStyle {
            case .present(let presenter, let animated):
                presenter.present(highSchoolsViewController, animated: animated)
            case .push(let navigationController, let animated):
                navigationController.pushViewController(highSchoolsViewController, animated: animated)
        }
        highSchoolsDataSource
            .dataPublisher
            .receive(on: DispatchQueue.main)
            .sink { [weak self] data in
                // We can do better usin Zip here
                self?.highSchoolsDataSource.loadSATScoresData()
                switch data.state {
                    case .loading:
                        self?.highSchoolsViewController.data.loading = true
                    case .finished(let data):
                        self?.handleData( data)
                    case .failed(let error):
                        self?.handle(error)
                    case .failedSATData(let error):
                        self?.handleFailedSATData(error)
                    case .initial:
                        break
                  
                }
            }
            .store(in: &subscribers)
        highSchoolsDataSource.loadData()
    }

    func handleData(_ data: [HighSchoolModel]) {
        highSchoolsViewController.data = .init(loading: false, models: data)
    }

}

extension HighSchoolsCoordinator: HighSchoolsViewControllerDelegate {
    func didTap(on schoolDBN: String, model: HighSchoolModel) {
        guard let data = highSchoolsDataSource.getSATScoreModel(for: schoolDBN) else {
            handleNoData()
            return
        }
        let viewController = SchoolDetailsViewController(data, model)
        navigationController.pushViewController(viewController, animated: true)
    }
}

// TODO: the alers should be  refactor and the literal strings should be localized
extension HighSchoolsCoordinator {
    
    func handleFailedSATData(_ error: Error) {
        highSchoolsViewController.data.loading = false
        let alert = UIAlertController(
            title: "An error occured",
            message: error.localizedDescription,
            preferredStyle: .alert
        )
        
        alert.addAction(UIAlertAction(
            title: "Dismiss",
            style: .default
        ))
        
        alert.addAction(UIAlertAction(
            title: "Retry",
            style: .default,
            handler: { [weak self] _ in
                self?.highSchoolsDataSource.loadSATScoresData()
            }
        ))
        
        highSchoolsViewController.present(alert, animated: true)
    }

    func handle(_ error: Error) {
        highSchoolsViewController.data.loading = false
        let alert = UIAlertController(
            title: "An error occured",
            message: error.localizedDescription,
            preferredStyle: .alert
        )
        
        alert.addAction(UIAlertAction(
            title: "Dismiss",
            style: .default
        ))
        
        alert.addAction(UIAlertAction(
            title: "Retry",
            style: .default,
            handler: { [weak self] _ in
                self?.highSchoolsDataSource.loadData()
            }
        ))
        
        highSchoolsViewController.present(alert, animated: true)
    }

    func handleNoData() {
        let alert = UIAlertController(
            title: "An error occured",
            message: "Sorry, we can't find any data for this school, please select another school.",
            preferredStyle: .alert
        )
        
        alert.addAction(UIAlertAction(
            title: "Dismiss",
            style: .default
        ))
        highSchoolsViewController.present(alert, animated: true)
    }
}
