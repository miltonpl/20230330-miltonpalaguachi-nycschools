//
//  HighSchoolsViewController.swift
//  NYCSchoolsInformation
//
//  Created by Milton Palaguachi on 3/29/23.
//

import UIKit
protocol HighSchoolsViewControllerDelegate: AnyObject {
    func didTap(on schoolID: String, model: HighSchoolModel)
}

extension HighSchoolsViewController {
    typealias Identifier = String
    
    enum Item: Hashable {
        case highschool(String)
    }

    struct Section: Hashable {
        let identifier: Identifier = UUID().uuidString
    }

    typealias DataSource = UICollectionViewDiffableDataSource<Section, Item>
    typealias Snapshot = NSDiffableDataSourceSnapshot<Section, Item>
}

class HighSchoolsViewController: UIViewController {
    enum Layout {
        static let heightDimension: CGFloat = 200
        static let itemFractionalWidth: CGFloat = 1.0
        static let itemSpacing: CGFloat = 20.0
        static let sectionSpacing: CGFloat = 10.0
    }

    weak var delegate: HighSchoolsViewControllerDelegate?

    private lazy var collectionView: UICollectionView = {
        let collectionView = UICollectionView(frame: view.bounds, collectionViewLayout: layout())
        collectionView.backgroundColor = .systemBackground
        collectionView.delegate = self
        return collectionView
    }()

    private lazy var loadingIndicator: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView(frame: view.bounds)
        indicator.style = .large
        indicator.color = .black
        indicator.hidesWhenStopped = true
        return indicator
    }()

    private lazy var dataSource = configureDataSource()

    var data: Data = .init() {
        didSet {
            applyData()
        }
    }

    init(title: String) {
        super.init(nibName: nil, bundle: nil)
        self.title = title
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        assemble()
    }

    func assembleView() {
        view.addAutolayoutView(loadingIndicator)
        view.addAutolayoutView(collectionView)
    }

    func assembleSubviewConstrains() {
        NSLayoutConstraint.activate([
            loadingIndicator.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            loadingIndicator.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            collectionView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            collectionView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            collectionView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
            collectionView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
        ])
    }

    private func applyData() {
        if data.loading {
            view.bringSubviewToFront(loadingIndicator)
            loadingIndicator.startAnimating()
        } else {
            loadingIndicator.stopAnimating()
            view.sendSubviewToBack(loadingIndicator)
        }
        applySections()
    }
  
    private func applySections(animated: Bool = false) {
        var snapshot = Snapshot()
        let highschoolSection = Section()
        snapshot.appendSections([highschoolSection])
        snapshot.appendItems(data.models.map{ .highschool($0.schoolName ?? "") }, toSection: highschoolSection)

        dataSource.apply(snapshot, animatingDifferences: animated)
    }
}

extension HighSchoolsViewController {
    
    private func layout() -> UICollectionViewLayout {
        let layout = UICollectionViewCompositionalLayout() { sectionIndex, layoutEnviroment in
            let configution = UICollectionLayoutListConfiguration(appearance: .plain)
            let section = NSCollectionLayoutSection.list(using: configution, layoutEnvironment: layoutEnviroment)
            let titleSize = NSCollectionLayoutSize(
                widthDimension: .fractionalWidth(Layout.itemFractionalWidth),
                heightDimension: .estimated(49)
            )
            let titleSupplementary = NSCollectionLayoutBoundarySupplementaryItem(
                layoutSize: titleSize,
                elementKind: SchoolDetailsHeaderView.identifier,
                alignment: .top
            )
            section.boundarySupplementaryItems = [titleSupplementary]
            return section
        }
        return layout
    }

    func configureDataSource() -> DataSource {
        let cellRegistration = UICollectionView.CellRegistration<UICollectionViewListCell, String> { cell, indexPath, item in
            var configuration = cell.defaultContentConfiguration()
            configuration.text = item
            cell.contentConfiguration = configuration
            cell.accessories = [.disclosureIndicator()]
        }

        let dataSource = DataSource(collectionView: collectionView) { collectionView, indexPath, item in
            switch item {
                case .highschool(let model):
                    return collectionView
                        .dequeueConfiguredReusableCell(using: cellRegistration, for: indexPath, item: model)
           }
        }
        let headerRegistration = sectionHeaderRegistration()
        dataSource.supplementaryViewProvider = { collectionView, _, indexPath in
            return collectionView.dequeueConfiguredReusableSupplementary(
                using: headerRegistration, for: indexPath
            )
        }
    
        return dataSource
    }
    
    private func sectionHeaderRegistration() -> UICollectionView.SupplementaryRegistration<SchoolDetailsHeaderView> {
        let headerRegistration = UICollectionView.SupplementaryRegistration<SchoolDetailsHeaderView>(
            elementKind: SchoolDetailsHeaderView.identifier) { supplementaryView, _, indexPath in
                supplementaryView.titlelabel.text = "Select your favorite school for more details."
                supplementaryView.titlelabel.font = .preferredFont(forTextStyle: .title2)
                supplementaryView.titlelabel.numberOfLines = 2
            }
        return headerRegistration
    }
}

extension HighSchoolsViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        guard data.models.count > 0, data.models.count > indexPath.item else { return }
        guard let dbn = data.models[indexPath.item].dbn else { return }
        delegate?.didTap(on: dbn, model: data.models[indexPath.item])
    }
}

extension HighSchoolsViewController {
    struct Data {
        var loading: Bool = false
        var models: [HighSchoolModel]

        init() {
            models = []
        }

        init(loading: Bool, models: [HighSchoolModel]) {
            self.loading = loading
            self.models = models
        }
    }
    // These funcs should be move a base class 
    func assemble() {
        assembleView()
        assembleSubviewConstrains()
    }
}
