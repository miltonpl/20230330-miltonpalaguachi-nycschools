//
//  HighSchoolDataSource.swift
//  NYCSchoolsInformation
//
//  Created by Milton Palaguachi on 3/29/23.
//

import Foundation
import Combine

enum Endpoints {
    static let nycSchoolsStringUrl = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
    static let nycSchoolSATStringUrl = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
}

protocol HighSchoolDataSourceAPI {
    var dataPublisher: AnyPublisher<HighSchoolDataSource.Data, Never> { get }
    func loadData()
    func loadSATScoresData()
    func getSATScoreModel(for dbn: String) -> SATScoreModel? 
}

class HighSchoolDataSource: HighSchoolDataSourceAPI {

    var dataPublisher: AnyPublisher<Data, Never> {
        $data.eraseToAnyPublisher()
    }
    
    
    @Published
    private var data: Data = .init(state: .initial)
    private var satScoreModel = [SATScoreModel]()
    private let networking: Networking
    private var subscribers = Set<AnyCancellable>()

    init(networking: Networking = .init(session: .shared)) {
        self.networking = networking
    }
    
    func loadData() {
        data.state = .loading
        guard let url = URL(string: Endpoints.nycSchoolsStringUrl) else { return }
        networking.request(request: .init(url: url), modelType: [HighSchoolModel].self)
            .receive(on: DispatchQueue.main)
            .sink { [weak self] completion in
                guard case .failure(let error)  = completion else {
                    return
                }
                self?.data.state = .failed(error)
            } receiveValue: { [weak self] models in
                self?.data.state = .finished(models)
            }
            .store(in: &subscribers)
    }

    func loadSATScoresData() {
        guard satScoreModel.isEmpty,
              let url = URL(string: Endpoints.nycSchoolSATStringUrl) else {
            return
        }
        networking.request(request: .init(url: url), modelType: [SATScoreModel].self)
            .receive(on: DispatchQueue.main)
            .sink { [weak self] completion in
                guard case .failure(let error)  = completion else {
                    return
                }
                self?.data.state = .failedSATData(error)
            } receiveValue: { [weak self] models in
                self?.satScoreModel = models
            }
            .store(in: &subscribers)
    }

    func getSATScoreModel(for dbn: String) -> SATScoreModel? {
        satScoreModel.first { $0.dbn == dbn}
    }
}

extension HighSchoolDataSource {
    enum State {
        case initial
        case loading
        case finished([HighSchoolModel])
        case failed(Error)
        case failedSATData(Error)
    }

    struct Data {
        var state: State
    }
}
