//
//  HighSchoolModel.swift
//  NYCSchoolsInformation
//
//  Created by Milton Palaguachi on 3/29/23.
//

import Foundation

// MARK: - HighSchoolModel
struct HighSchoolModel: Codable {
    var dbn, schoolName, overviewParagraph, school10ThSeats: String?
    var academicopportunities1, academicopportunities2, ellPrograms, location: String?
    var phoneNumber, faxNumber, schoolEmail, website: String?
    var subway, bus, extracurricularActivities, schoolSports: String?
    var primaryAddressLine1, city, zip, stateCode: String?
    var bin, bbl, nta, borough: String?

    enum CodingKeys: String, CodingKey {
        case dbn
        case schoolName = "school_name"
        case overviewParagraph = "overview_paragraph"
        case school10ThSeats
        case academicopportunities1, academicopportunities2
        case ellPrograms
        case location
        case phoneNumber = "phone_number"
        case faxNumber = "fax_number"
        case schoolEmail = "school_email"
        case website, subway, bus
        case extracurricularActivities = "extracurricular_activities"
        case schoolSports
        case primaryAddressLine1 = "primary_address_line_1"
        case city, zip
        case stateCode = "state_code"
        case bin, bbl, nta, borough
    }

    var models: [HighSchoolCell.Model] {
        var models = [HighSchoolCell.Model]()
        models.append(.init(title: "School Name", subtitle: schoolName ?? "Unknown"))
        models.append(.init(title: "Address", subtitle: address))
        models.append(.init(title: "Phone", subtitle: phoneNumber))
        models.append(.init(title: "Fax", subtitle: faxNumber))
        models.append(.init(title: "Email", subtitle: schoolEmail))
        models.append(.init(title: "Overview", subtitle: overviewParagraph ?? "Unknown"))
        models.append(.init(title: "Extracurricular Cctivities", subtitle: extracurricularActivities))
        models.append(.init(title: "Academic Oppornutity", subtitle: academicopportunities1 ?? "Unknown"))
     return models
    }
    
    var address: String {
        var address: String = ""
        address += (primaryAddressLine1 ?? "") + ", "
        address += (city ?? "") + ", "
        address += (zip ?? "") + ", "
        address += (stateCode ?? "")
        return address
    }
}
