//
//  SATScoreModel.swift
//  NYCSchoolsInformation
//
//  Created by Milton Palaguachi on 3/30/23.
//

import Foundation

// MARK: - WelcomeElement

struct SATScoreModel: Codable {
    var dbn, schoolName, numOfSatTestTakers, satCriticalReadingAvgScore: String?
    var satMathAvgScore, satWritingAvgScore: String?

    enum CodingKeys: String, CodingKey {
        case dbn
        case schoolName = "school_name"
        case numOfSatTestTakers = "num_of_sat_test_takers"
        case satCriticalReadingAvgScore = "sat_critical_reading_avg_score"
        case satMathAvgScore = "sat_math_avg_score"
        case satWritingAvgScore = "sat_writing_avg_score"
    }

    var satScoreModel: [SATScoreCell.Model] {
        var list = [SATScoreCell.Model]()
        if let satMathAvgScore = satMathAvgScore {
            list.append(.init(title: "Math", subtitle: satMathAvgScore))
        }
        if let readingAvgScore = satCriticalReadingAvgScore {
            list.append(.init(title: "Critical Reading", subtitle: readingAvgScore))
        }
        if let satWritingAvgScore = satWritingAvgScore {
            list.append(.init(title: "Writting", subtitle: satWritingAvgScore))
        }
        if let numOfSatTestTakers = numOfSatTestTakers {
            list.append(.init(title: "Test Taker", subtitle: numOfSatTestTakers))
        }
        return list
    }
}
