//
//  AppCoordinator.swift
//  NYCSchoolsInformation
//
//  Created by Milton Palaguachi on 3/29/23.
//

import UIKit


class StartAppCoordinator: Coordinator {
    private let window: UIWindow
    private let navitionController = UINavigationController()

    private lazy var highSchoolsCoordinator: HighSchoolsCoordinator = {
        .init(presentingStyle: .push(navigationController: navitionController, animated: true))
    }()

    init(window: UIWindow = UIWindow(frame: UIScreen.main.bounds)) {
        self.window = window
    }

    func start() {
        setupWindow()
        highSchoolsCoordinator.start()
    }

    private func setupWindow() {
        window.rootViewController = navitionController
        window.makeKeyAndVisible()
        window.rootViewController?.view.backgroundColor = .white
    }
}
