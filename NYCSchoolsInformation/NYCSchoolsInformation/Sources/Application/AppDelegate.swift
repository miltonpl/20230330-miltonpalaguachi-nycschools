//
//  AppDelegate.swift
//  NYCSchoolsInformation
//
//  Created by Milton Palaguachi on 3/29/23.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    var startAppCoordinator: StartAppCoordinator?

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        startAppCoordinator = StartAppCoordinator(
            window: UIWindow(frame: UIScreen.main.bounds)
        )
        startAppCoordinator?.start()
        return true
    }

    func handleEvent(_ event: UIEvent) { }
}
