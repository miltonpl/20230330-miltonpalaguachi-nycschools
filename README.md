# 20230330-MiltonPalaguachi-NYCSchools

---
## Capabilities
- List NYC Schools
- Shows SAT Scores and School Details

## About
- This App shows the NYC High Schools
- Up on selecting a school the app shows the the SAT Scores and Schoold Details

# Future Work
 - Add search bar so that user can find a school
 - Add Link for school websize
 - Add animation
 - UX Design
 - Add Swiftlint to improve code quality
 - Refactor/Clean up code fallowing the DRY Principles and SOLID priciples.

## Utility
- UIVIew
- UIViewControllers
- UICollectionViewListCell
- UICollectionViewDiffableDataSource
- URLSession
- @Published
- AnyCancellable
- Coordinator Pattern
---

## Sample
![Error|100x50](https://gitlab.com/miltonpl/20230330-miltonpalaguachi-nycschools/-/blob/main/photos/error.png)
![Schools|100x50](https://gitlab.com/miltonpl/20230330-miltonpalaguachi-nycschools/-/blob/main/photos/schools.png)
![Lauch|100x50](https://gitlab.com/miltonpl/20230330-miltonpalaguachi-nycschools/-/blob/main/photos/launch.png)
![Details|100x50](https://gitlab.com/miltonpl/20230330-miltonpalaguachi-nycschools/-/blob/main/photos/school_details.png)

# Prerequisites
Xcode 
Version 14.1
# Minimum Deployment Version
iOS 14.0

# Installation
- Clone the repo
- https://gitlab.com/miltonpl/20230330-miltonpalaguachi-nycschools.git

## Authors and acknowledgment
Milton Palaguachi

# Contact
miltonpalaguachi@gmail.com
